package com.zoke.phone.helper.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.zoke.phone.helper.R;

public abstract class BaseDialog extends Dialog {

    protected LayoutInflater mInflater;

    public BaseDialog(@NonNull Context context) {
        super(context, R.style.BaseDialogStyle);
        init(context);
    }

    protected void init(Context context) {
        mInflater = LayoutInflater.from(context);
        setContentView(getLayoutRes());
        windowDeploy(context);
    }


    protected <T extends View> T find(@IdRes int id) {
        return (T) findViewById(id);
    }


    private void windowDeploy(Context context) {
        WindowManager m = ((Activity) context).getWindowManager();
        Display d = m.getDefaultDisplay();
        Window window = getWindow(); //
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.width = d.getWidth();
        window.setAttributes(wl);
    }


    protected abstract int getLayoutRes();
}
