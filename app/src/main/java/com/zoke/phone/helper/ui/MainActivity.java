package com.zoke.phone.helper.ui;

import android.os.Bundle;
import android.view.View;

import com.zoke.phone.helper.R;
import com.zoke.phone.helper.base.BaseActivity;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;

import io.nebulas.Constants;
import io.nebulas.api.SmartContracts;
import io.nebulas.model.GoodsModel;
import io.nebulas.utils.Util;

@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    private String serialNumber = "";
    private String value = "100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Event({R.id.tv_hello})
    private void onViewClick(View view) {
        nasPay(view);
    }


    public void nasPay(View view) {
        if (view == null) {
            return;
        }
        serialNumber = Util.getRandomCode(Constants.RANDOM_LENGTH);
        String to = "n1ULQeCi1FEbDn4tktufhzZXCTnv4eJQb4C";//入账钱包地址，钱包地址，钱包地址
        GoodsModel goods = new GoodsModel();
        goods.name = "Nebulas";
        goods.desc = "星云链，下一代共联";
        SmartContracts.pay(this, Constants.MAIN_NET, goods, to, value, serialNumber);
    }

}
