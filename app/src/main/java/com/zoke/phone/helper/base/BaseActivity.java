package com.zoke.phone.helper.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.gyf.barlibrary.ImmersionBar;

import org.xutils.x;

public class BaseActivity extends AppCompatActivity {

    protected ImmersionBar mImmersionBar;

    public void open(Activity activity, Class<?> cls) {
        activity.startActivity(new Intent(activity, cls));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
        if (isImmersionBarEnabled())
            initImmersionBar();
    }


    protected boolean isSlide() {
        return false;
    }

    protected boolean isImmersionBarEnabled() {
        return true;
    }


    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.init();
    }

    @Override
    protected void onDestroy() {
        if (mImmersionBar != null) mImmersionBar.destroy();
        super.onDestroy();
    }
}
