package io.nebulas.action;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import io.nebulas.Constants;
import io.nebulas.utils.Util;


/**
 * Created by donald99 on 18/5/23.
 */

public class ContractAction {

    /**
     * Schema 方式启动星云钱包
     *
     * @param context 上下文
     * @param url     schema
     */
    public static void start(Context context, String url) {
        if (context == null || TextUtils.isEmpty(url)) {
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception e) {
            handleException(context);
        }
    }

    private static void handleException(Context context) {
        boolean isAppInstalled = Util.isAppInstalled(context, Constants.NAS_NANO_PACKAGE_NAME);
        if (!isAppInstalled) {
            //没有安装
            Util.openBrowser(context, Constants.NAS_NANO_APK_URL);
            return;
        }
    }


}

